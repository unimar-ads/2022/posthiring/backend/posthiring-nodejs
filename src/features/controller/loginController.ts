import { Request, Response } from 'express';

const loginModel = require('../model/login')

export default {
  async Auth(request: Request, response: Response) {
    try {
      const { nickname, password } = request.body.data
      
      const [login] = await loginModel.Auth(nickname, password)

      const retorno = {
        retorno: {
          user_id: login.user_id,
          user_name: login.user_name,
          sector_id: login.sector_id,
          document_id: login.document_id,
          is_supervisor: login.is_supervisor,
          nickname: login.nickname,
          email: login.email,
          logged: true
        }
      }
      response.status(201).json(retorno)
    } catch (err) {
      const msg = {
        retorno: {
          mensagem: "Usuário não autenticado",
          logged: false,
          erro: err
        }
      }
      response.status(400).json(msg)
    }
  }
}