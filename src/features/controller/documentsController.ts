import { Request, Response } from 'express';

const documentsModel = require('../model/documents')

export default {
	async listAllDocuments(request: Request, response: Response) {
		try {
			const documents = await documentsModel.listAllDocuments()

			response.status(200).json(documents)
		} catch (error) {
			response.status(500).json({ mensagem: "Não foi buscar o documento", erro: error })
		}
	},

	async listOneDocuments(request: Request, response: Response) {
		try {
			const { sector_id } = request.params

			const documents = await documentsModel.listOneDocuments(sector_id)

			response.status(200).json({ documents: documents })
		} catch (error) {
			response.status(500).json({ mensagem: "Não foi buscar o documento", erro: error })
		}
	},

	async insertDocuments(request: Request, response: Response) {
		try {
			const { title, material_link, document_content, sector_id } = request.body

			const documents = await documentsModel.insertDocuments(title, material_link, document_content, sector_id)

			response.status(201).json({ mensagem: "Cadastro realizado com sucesso", documents: documents })
		} catch (error) {
			response.status(500).json({ message: "Não foi possível cadastrar o documento", erro: error })
		}
	},

	async deleteDocuments(request: Request, response: Response) {
		try {
			const { document_id } = request.params

			const documents = await documentsModel.deleteDocuments(document_id)

			response.status(201).json({ mensagem: "Documento Deletado", documents: documents })
		} catch (error) {
			response.status(500).json({ mensagem: "Não foi possível deletar o documento", erro: error })
		}
	},

	async updateDocuments(request: Request, response: Response) {
		try {
			const { document_id } = request.params

			const { title, material_link, document_content, sector_id } = request.body

			const documents = await documentsModel.updateDocuments(document_id, title, material_link, document_content, sector_id)

			response.status(201).json({ mensagem: "Documento atualizado com sucesso", documents: documents })
		} catch (error) {
			response.status(500).json({ mensagem: "Não foi possível atualizar o documento", erro: error })
		}
	},
}