import { Request, Response } from 'express';

const sectorModel = require('../model/sector')

export default {
  async listAllSector(request: Request, response: Response) {
    try {
      const sector: [] = await sectorModel.listAllSector()
  
      response.status(200).json(sector)   
    } catch (error) {
      response.status(500).json({ message: "Não foi possível buscar o setor", erro: error })
    }
  },

  async insertSector(request: Request, response: Response) {
    try {
      const { sector_name, description } = request.body.data

      const sector = await sectorModel.insertSector(sector_name, description)

      response.status(201).json({ message: "Cadastro realizado com sucesso", sector })
    } catch (error) {
      response.status(500).json({ message: "Não foi possível cadastrar o setor", erro: error })
    }
  },

  async deleteSector(request: Request, response: Response) {
    try {
      const { sector_id } = request.params

      const sector = await sectorModel.deleteSector(sector_id)

      response.status(201).json({ mensagem: "Setor Deletado com sucesso", sector: sector })
    } catch (error) {
      response.status(500).json({ mensagem: "Não foi possível deletar o setor", erro: error })
    }
  },

  async updateSector(request: Request, response: Response) {
    try {
      const { sector_id } = request.params

      const { sector_name, description } = request.body

      const sector = await sectorModel.updateSector(sector_id, sector_name, description)

      response.status(201).json({ mensagem: "Setor atualizado com sucesso", sector: sector })
    } catch (error) {
      response.status(500).json({ mensagem: "Não foi possível atualizar o setor", erro: error })
    }
  },

  async listOneSector(request: Request, response: Response) {
    try {
      const { sector_id } = request.params

      const result = await sectorModel.listOneSector(sector_id)

      if (!result.length) {
        response.status(400).json({ messagem: 'Setor não encontrado' })
      }

      const [sector] = result

      response.status(200).json({ sector })
    } catch (error) {
      response.status(500).json({ mensagem: "Não foi possivel buscar o setor informado", erro: error })
    }
  },
}

