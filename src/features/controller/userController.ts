import { Request, Response } from 'express';

const userModel = require('../model/usuario')

export default {
  async listAllUser(request: Request, response: Response) {
    try {
      const users = await userModel.listAllUser()

      response.status(200).json({ mensagem: "Busca realizada com sucesso", users: users })
    } catch (error) {
      response.status(500).json({ mensagem: "Não foi possível realizar a buscar", erro: error })
    }
  },

  async listOneUser(request: Request, response: Response) {
    try {
      const { user_id } = request.params

      const users = await userModel.listOneUser(user_id)

      response.status(200).json({ mensagem: "Busca realizada com sucesso", users: users })
    } catch (error) {
      response.status(500).json({ mensagem: "Não foi possível realizar a buscar", erro: error })
    }
  },

  async listProfile(request: Request, response: Response) {
    try {
      const { user_id } = request.params
      console.log(user_id)
      const users = await userModel.listProfile(user_id)

      response.status(200).json({ mensagem: "Busca realizada com sucesso", users: users })
    } catch (error) {
      response.status(500).json({ mensagem: "Não foi possível realizar a buscar", erro: error })
    }
  },

  async insertUsuarioAdm(request: Request, response: Response) {
    try {
      const { user_name, birth_date, sector_id, is_supervisor, nickname, email, password } = request.body

      const [users] = await userModel.insertUsuario(user_name, birth_date, sector_id, is_supervisor, nickname, email, password)

      const userAdm = await userModel.insertUsuarioAdm(users.user_id)

      response.status(201).json({ mensagem: "Cadastro realizado com sucesso", userAdm: { userAdm } })
    } catch (error) {
      response.status(500).json({ mensagem: "Erro ao cadastrar", erro: error })
    }
  },

  async insertUsuarioAssociate(request: Request, response: Response) {
    try {
      const { user_name, birth_date, sector_id, is_supervisor, nickname, email, password, supervisor_id } = request.body

      const [users] = await userModel.insertUsuario(user_name, birth_date, sector_id, is_supervisor, nickname, email, password)

      const user_associate = await userModel.insertUsuarioAssociate(users.user_id, supervisor_id)

      response.status(201).json({ mensagem: "Cadastro realizado com sucesso", user_associate: { user_associate } })
    } catch (error) {
      response.status(500).json({ mensagem: "Erro ao cadastrar", erro: error })
    }
  },

  async updateUsuario(request: Request, response: Response) {
    try {
      const { user_id } = request.params

      const { user_name, birth_date, sector_id, nickname, email, password } = request.body

      const users = await userModel.updateUsuario(user_name, birth_date, sector_id, nickname, email, password, user_id)

      response.status(201).json({ mensagem: "Atualização realizada com sucesso", users: users })
    } catch (error) {
      response.status(500).json({ mensagem: "Erro ao atualizar", erro: error })
    }
  },

  async updateUsuarioAssociate(request: Request, response: Response) {
    try {
      const { user_associate_id } = request.params

      const { supervisor_id } = request.body

      const users = await userModel.updateUsuarioAssociate(user_associate_id, supervisor_id)

      response.status(201).json()
    } catch (error) {
      response.status(500).json({ mensagem: "Erro ao atualizar", erro: error })
    }
  },

  async listAllUserAdm(request: Request, response: Response) {
    const users = await userModel.listAllUserAdm()
    response.json(users)
  },

  async listAllUserAssociate(request: Request, response: Response) {
    const users = await userModel.listAllUserAssociate()
    response.json(users)
  },

  async altStatus(request: Request, response: Response) {
    try {
      const { user_id } = request.params

      const users = await userModel.altStatus(user_id)

      response.status(201).json({ mensagem: "Atualização realizada com sucesso", users: users })
    } catch (error) {
      response.status(500).json({ mensagem: "Erro ao atualizar", erro: error })
    }
  },
}

