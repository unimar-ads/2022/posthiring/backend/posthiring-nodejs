const usuarioConnection = require('../../database/connection')

async function listAllUser() {
  try {
    const sql = `select * from public.user`

    const res = await usuarioConnection.client.query(sql);

    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function listOneUser(user_id: number) {
  try {
    const sql = `select * from public.user as u where u.user_id = $1;`

    const values = [user_id]

    const res = await usuarioConnection.client.query(sql, values);

    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function listProfile(user_id: number) {
  console.log(user_id || 'entrei')
  try {
    const sql = `
    select
      s.sector_name,
      u.user_name,
      to_char(u.birth_date, 'dd/mm/yyyy') as data_nascimento,
      u.email,
      u.is_supervisor
    from
      public.user as u
    left join public.sector as s on
      u.sector_id = s.sector_id 
    where
      u.user_id = $1`

    const values = [user_id]

    const res = await usuarioConnection.client.query(sql, values);

    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function insertUsuario(user_name: string, birth_date: Date, sector_id: number, is_supervisor: boolean, nickname: string, email: string, password: string) {
  try {
    const sql = `insert into
                  public.user (
                  user_name, birth_date, sector_id, is_supervisor, nickname, email, password
                )
                values (
                  $1, $2, $3, $4, $5, $6, $7
                ) 
                returning 
                  user_id`

    const values = [user_name, birth_date, sector_id, is_supervisor, nickname, email, password]

    const res = await usuarioConnection.client.query(sql, values);

    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function insertUsuarioAdm(user_id: string) {
  try {
    const sql = `insert into public.user_adm (user_id) values ($1) returning user_adm_id`

    const values = [user_id]

    const res = await usuarioConnection.client.query(sql, values);
    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function insertUsuarioAssociate(user_id: string, supervisor_id: string) {
  try {
    const sql = `insert into public.user_associate (user_id, supervisor_id) values ($1,$2) returning user_associate_id`

    const values = [user_id, supervisor_id]

    const res = await usuarioConnection.client.query(sql, values);

    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function updateUsuario(user_name: string, birth_date: Date, sector_id: number, nickname: string, email: string, password: string, user_id: number) {
  try {
    const sql = `update public.user set  user_name = $1, birth_date = $2, sector_id = $3, nickname = $4, email = $5, password = $6 
                where user_id = $7;`

    const values = [user_name, birth_date, sector_id, nickname, email, password, user_id]

    const res = await usuarioConnection.client.query(sql, values);
    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function updateUsuarioAssociate(user_associate_id: string, supervisor_id: string) {
  try {
    const sql = `update public.user_associate set  supervisor_id = $1 where user_associate_id = $2;`

    const values = [supervisor_id, user_associate_id]

    const res = await usuarioConnection.client.query(sql, values);
    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function altStatus(user_id: string) {
  try {
    const sql = `update public.user set status = false where user_id = $1;`

    const values = [user_id]

    const res = await usuarioConnection.client.query(sql, values);
    return res.rows;
  } catch (error) {
    return (error);
  }
}

async function listAllUserAdm() {
  const sql = `select * from user_adm a left join public.user u on a.user_id = u.user_id`

  try {
    const res = await usuarioConnection.client.query(sql);
    return res.rows;
  } catch (err) {
    console.error(err);
  }
}

async function listAllUserAssociate() {
  const sql = `select * from user_associate ass left join public.user as u on ass.user_id = u.user_id`

  try {
    const res = await usuarioConnection.client.query(sql);
    return res.rows;
  } catch (err) {
    console.error(err);
  }
}
module.exports = 
{ 
  listAllUser,
  insertUsuario, 
  insertUsuarioAdm, 
  insertUsuarioAssociate, 
  updateUsuarioAssociate, 
  updateUsuario, 
  altStatus, 
  listOneUser, 
  listProfile, 
  listAllUserAdm, 
  listAllUserAssociate 
}