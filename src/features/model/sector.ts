const sectorConnection = require('../../database/connection')

async function listAllSector() {
	try {
		const sql = `select * from public.sector`

		const res = await sectorConnection.client.query(sql);

		return res.rows;
	} catch (error) {
		return (error);
	}
}

async function insertSector(sector_name: string, description: string) {
	try {
		const sql = `insert into public.sector (sector_name, description) values ($1, $2) returning sector_id`

		const values = [sector_name, description]

		const res = await sectorConnection.client.query(sql, values);
		return res.rows;
	} catch (error) {
		return (error);
	}
}

async function deleteSector(sector_id: number) {
	try {
		const sql = `delete from public.sector where sector.sector_id = $1`

		const values = [sector_id]

		const res = await sectorConnection.client.query(sql, values);

		return res.rows;
	} catch (error) {
		return (error);
	}
}

async function updateSector(sector_id: number, sector_name: string, description: string) {
	try {
		const sql = `UPDATE public.sector SET  sector_name = $1, description = $2 where sector_id = $3;`

		const values = [sector_name, description, sector_id]

		const res = await sectorConnection.client.query(sql, values);

		return res.rows;
	} catch (error) {
		return (error);
	}
}

async function listOneSector(sector_id: number) {
	try {
		const sql = 'select * from sector where sector_id = $1;'

		const values = [sector_id]

		const res = await sectorConnection.client.query(sql, values);
		return res.rows;
	} catch (error) {
		return (error);
	}
}

module.exports = { listAllSector, insertSector, deleteSector, updateSector, listOneSector }