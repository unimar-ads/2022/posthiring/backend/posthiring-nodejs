import { Router } from "express";
const routes = Router();

import documentsController from '../features/controller/documentsController';
import loginController from '../features/controller/loginController';
import sectorController from '../features/controller/sectorController';
import userController from '../features/controller/userController';

//Autenticação.
routes.post('/api/login/Auth', loginController.Auth)

//Listagem de todos os usuários.
routes.get('/api/user/listAllUser', userController.listAllUser)

//listagem de usuário Adm
routes.get('/api/user/listAllUserAdm', userController.listAllUserAdm)

//listagem de usuário Adm
routes.get('/api/user/listAllUserAssociate', userController.listAllUserAssociate)

//Listagem do perfil do usuário.
routes.get('/api/user/profile/:user_id', userController.listProfile)

//Listagem de apenas um usuário.
routes.get('/api/user/listOneUser/:user_id', userController.listOneUser)

//Registro de usuário administrador.
routes.post('/api/user/insertUser/userAdm', userController.insertUsuarioAdm)

//Registro de usuário Associate.
routes.post('/api/user/insertUser/userAssociate', userController.insertUsuarioAssociate)

//Edição de usuário Associate.
routes.put('/api/user/updateUser/user/:user_id', userController.updateUsuario)

//Edição de usuário Associate
routes.put('/api/user/updateUser/userAssociate/:user_associate_id', userController.updateUsuarioAssociate)

//Delete / Inativação de usuário.
routes.put('/api/user/delete/user/:user_id', userController.altStatus)

//listagem de todos os documentos.
routes.get('/api/documents/listAllDocuments', documentsController.listAllDocuments)

//listagem de apenas um documento.
routes.get('/api/documents/listOneDocuments/:sector_id', documentsController.listOneDocuments)

//Registro de documento.
routes.post('/api/documents/insertDocuments', documentsController.insertDocuments)

//Delete de documento.
routes.delete('/api/documents/deleteDocuments/:document_id', documentsController.deleteDocuments)

//Edição de documento.
routes.put('/api/documents/updateDocuments/:document_id', documentsController.updateDocuments)

//listagem de apenas um setor.
routes.get('/api/sector/listOneSector/:sector_id', sectorController.listOneSector)

//Listagem de todos setores.
routes.get('/api/sector/listAllSector', sectorController.listAllSector)

//Registro de setor.
routes.post('/api/sector/insertSector', sectorController.insertSector)

//Delete de setor.
routes.delete('/api/sector/deleteSector/:sector_id', sectorController.deleteSector)

//Edição de setor.
routes.put('/api/sector/updateSector/:sector_id', sectorController.updateSector)

export default routes;